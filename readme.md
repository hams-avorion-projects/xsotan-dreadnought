# Xsotan Dreadnought

The Xsotan Dreadnought is a boss with very strong shields. It can charge up during fight wich repairs its shields and increase the damage output. Also he will call more xsotan ships to help him.

He got some kind of special shields that absorbs ionized projectils, shield breaking weapons and even collision damage.


### Usage

By default this mod is a random event that will be automaticly started about every 2-3 hours for users (similar to the distress call mission).

##### Custom initialization (for modders)

By default the XsotanDreadnought will appear as a random event.

You can also use this mod as a resource that can be implemented in your own mods/scripts.

`Player():addScript("data/scripts/player/XsotanDreadnoughtAttack.lua", true)`
or
`Player():addScriptOnce("data/scripts/player/XsotanDreadnoughtAttack.lua", true)`

Calling this will start the event for the player. In case you do not need the random event for your server you can disable it in the config file.

For testing/developing I recommand using this console command ingame. It will start the event.

`/run Player():addScript("data/scripts/player/XsotanDreadnoughtAttack.lua", true)`


### Supported languages
Included languages:

- English
- German
- Russian

If you miss your language feel free to cantact me, to help me translate this mod in your language.


## Configuration
You should take a look into the config file inside of your galaxy folder.


#### Credits for translations
Thanks to all who helped translating this mod:

* Russian by Rinart73

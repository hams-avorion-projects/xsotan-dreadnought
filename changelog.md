# Changelog
### 1.2.0
* Updated mod for Avorion 2.x
* Cleaned the code
* Reduced damage multiplier in default config (only affects new galaxies)

### 1.1.5
* Improved mod compatibility

### 1.1.4
* Updated mod for Avorion 1.0

### 1.1.3
* Updated for Avorion 0.29
* Fixed an issue where the random event happened too often

### 1.1.2
* Updated for Avorion 0.26+

### 1.1.1
* Added priority to Dreadnought for Carrier Command mod

### 1.1.0
* Updated mod for ConfigLib 0.3 and Avorion 0.23

### 1.0.1
* Fixed wrong config filename and mod ID

### 1.0.0
* Update mod for workshop support
* Replace custom config by ConfigLib mod

### 0.4.2
* Updated mod for Avorion 0.20
* Changed version for optional entitydbg.lua to match Avorion version 


### 0.4.1
* Fixed an issue where the Dreadnought may drop Avorion in the very outside of the galaxy

### 0.4.0
* Improved compatibility to [Ham Galaxy Settings](https://www.avorion.net/forum/index.php/topic,5056.0.html) mod.
* Improved/fixed calculations of the damage the Dreadnought deals. This may affect difficulty. You should reconfigure it for your galaxy. Watch comments in config file.
* Added the Xsotan upscale function that came with Avorion 0.18.0 to scale the Dreadnought (can be disabled in config file).


### 0.3.0
* Moved project to GitLab
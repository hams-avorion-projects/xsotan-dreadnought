
meta =
{
    id = "1721567838",
    name = "XsotanDreadnought",
    title = "Xsotan Dreadnought",
    description = "A custom Xsotan mini boss wich makes the everyday life more challanging.",
    authors = {"Hammelpilaw"},
    version = "1.2.1",
    dependencies = {
        {id = "1720259598", min = "0.3.1"},
        {id = "Avorion", max = "2.*"},
        {id = "1741735681", min = "1.12", optional = true}
    },

    -- Set to true if the mod only has to run on the server. Clients will get notified that the mod is running on the server, but they won't download it to themselves
    serverSideOnly = false,

    -- Set to true if the mod only has to run on the client, such as UI mods
    clientSideOnly = false,

    -- Set to true if the mod changes the savegame in a potentially breaking way, as in it adds scripts or mechanics that get saved into database and no longer work once the mod gets disabled
    -- logically, if a mod is client-side only, it can't alter savegames, but Avorion doesn't check for that at the moment
    saveGameAltering = false,

    -- Contact info for other users to reach you in case they have questions
    contact = "info@scrap-yard.org",
}

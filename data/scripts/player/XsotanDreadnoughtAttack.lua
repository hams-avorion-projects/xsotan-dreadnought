package.path = package.path .. ";data/scripts/lib/?.lua"
package.path = package.path .. ";data/scripts/?.lua"
package.path = package.path .. ";data/scripts/lib/ConfigLib/?.lua"


include ("galaxy")
include ("randomext")
include ("utility")
include ("stringutility")
include ("defaultscripts")
include ("mission")
include ("callable")


missionData.brief = "Curious subspace signals"%_t
missionData.title = "Curious subspace signals"%_t
missionData.description = "The ${giver} asked you to investigate the source of some curious subspace in sector (${location.x}:${location.y})."%_t


local SectorTurretGenerator = include ("sectorturretgenerator")
local UpgradeGenerator = include ("upgradegenerator")
local ShipUtility = include ("shiputility")
local PlanGenerator = include ("plangenerator")
local SectorSpecifics = include ("sectorspecifics")
local Xsotan = include ("data/scripts/lib/story/xsotan")

local ConfigLib = include ("ConfigLib")
local XDConfigLib = ConfigLib("1721567838")

local generated = 0
local timeSinceCall = 0
local customBulletin = nil -- Used to get target sector when using custom initialization


function getUpdateInterval()
    return 1
end


function initialize(firstInitialization)
	
	XDConfigLib.log(4, "Initialize mission")
	if onClient() then
		XDConfigLib.log(4, "Initialize client")
		invokeServerFunction("sendCoordinates")
	else
		XDConfigLib.log(4, "Initialize server")
		local player = Player()
		
		
		missionData.isEvent = true
		
		local specs = SectorSpecifics()
		local x, y = Sector():getCoordinates()
		local coords = specs.getShuffledCoordinates(random(), x, y, 7, 25)
		
		for _, coord in pairs(coords) do
			local regular, offgrid, blocked, home = specs:determineContent(coord.x, coord.y, Server().seed)

			if not regular and not offgrid and not blocked and not home and not (coord.x == 0 and coord.y == 0) then
				missionData.location = {x=coord.x, y=coord.y}
				
				if not Galaxy():sectorExists(coord.x, coord.y) then
					break
				end
			end
		end
		
		-- if no empty sector could be found, exit silently
		if not missionData.location then
			XDConfigLib.log(3, "Could not find sector for custom initialization - terminate")
			terminate()
		end
		
		if firstInitialization then
			player:sendChatMessage("", 0, "Your sensors picked up very curious subspace signals at \\s(%i:%i)."%_t, missionData.location.x, missionData.location.y)
			player:sendChatMessage("", 3, "Your sensors picked up subspace signals at %i:%i."%_t, missionData.location.x, missionData.location.y)
		end
		


		
		player:registerCallback("onSectorEntered", "onSectorEntered")
		player:registerCallback("onSectorLeft", "onSectorLeft")

		
	end
end

function updateServer(timeStep)
    local x, y = Sector():getCoordinates()
    if generated == 0 then
        timeSinceCall = timeSinceCall + timeStep

        if timeSinceCall > 30 * 60 then
            terminate()
        end
    end
end

function onSectorLeft(player, x, y)
	if x ~= missionData.location.x or y ~= missionData.location.y then return end
	
	--delete the Xsotan Dreadnought and terminate.
	local sector = Sector()
	
	local entities = {sector:getEntities()}
	for _, entity in pairs(entities) do
		if entity:hasScript("data/scripts/entity/XsotanDreadnought.lua") then
			sector:deleteEntity(entity)
		end
	end
	
    terminate()
end

function onSectorEntered(player, x, y)

    if x ~= missionData.location.x or y ~= missionData.location.y then return end

	if generated == 0 then
		boss = createXsotanDreadnought()
		boss:registerCallback("onDestroyed", "onBossDestroyed")
	end
	generated = 1
end

function createXsotanDreadnought()
	XDConfigLib.log(4, "Create Dreadnought")
	local x, y = Sector():getCoordinates()
    local position = Matrix()
	local dist = length(vec2(x, y))
	
    local volume = Balancing_GetSectorShipVolume(x, y)
	
	local coreFactor = 1
	if dist < XDConfigLib.get("coreDistance") then
		coreFactor = XDConfigLib.get("bossVolumeFactorCore") * (1 - (dist / XDConfigLib.get("coreDistance")))
	end

    volume = volume * XDConfigLib.get("bossVolumeFactor") * coreFactor

    
    local probabilities = Balancing_GetMaterialProbability(x, y)
	
	local material = Material(getValueFromDistribution(probabilities))
    
	
    local faction = Xsotan.getFaction()
	
	-- Create the dreadloch
	local plan = createPlan(volume, material)
	
	local ship = createShip(faction, "", plan, position, EntityArrivalType.Jump)
	
	
	local numTurrets = math.max(2, Balancing_GetEnemySectorTurrets(x, y))
	-- Lets add 2 random turret types
	for i = 1, 2 do
		local turret = SectorTurretGenerator():generateArmed(x, y, 0, Rarity(RarityType.Rare))
		local weapons = {turret:getWeapons()}
		turret:clearWeapons()
		for _, weapon in pairs(weapons) do
			weapon.reach = XDConfigLib.get("weaponRange") * 100
			if weapon.isBeam then
				weapon.blength = XDConfigLib.get("weaponRange") * 100
			else
				weapon.pmaximumTime = weapon.reach / weapon.pvelocity
			end
			turret:addWeapon(weapon)
		end

		turret.coaxial = false
		ShipUtility.addTurretsToCraft(ship, turret, numTurrets)
	end
	ShipUtility.addBossAntiTorpedoEquipment(ship)
	
	if XDConfigLib.get("useTorps") or (XDConfigLib.get("useTorpsCore") and dist < XDConfigLib.get("coreDistance")) then
		ShipUtility.addTorpedoBoatEquipment(ship)
	end
	
	if XDConfigLib.get("upScale") then
		if Xsotan.applyCenterBuff ~= nil then
			Xsotan.applyCenterBuff(ship)
		else
			Xsotan.upScale(ship)
		end
	end
	
	XDConfigLib.log(4, "Initial damage multiplier:", ship.damageMultiplier)
	
	local coreMulti = 1
	if dist < XDConfigLib.get("coreDistance") and XDConfigLib.get("damageMultiplierCore") > 1 then
		coreMulti = XDConfigLib.get("damageMultiplierCore") * (1 - (dist / XDConfigLib.get("coreDistance")))
	end
	ship.damageMultiplier = ship.damageMultiplier * XDConfigLib.get("damageMultiplier") * coreMulti
	
	XDConfigLib.log(4, "Total damage multiplier:", ship.damageMultiplier)

	ship.title = "Xsotan Dreadnought"%_t
    ship.crew = ship.minCrew
	
	-- Reduce automatic shield recharge and movement speed, its annoying when it always moves directly in front of you...
	ship:addBaseMultiplier(StatsBonuses.Velocity, -0.7)
    ship:addBaseMultiplier(StatsBonuses.Acceleration, -0.7)
	ship:addBaseMultiplier(StatsBonuses.ShieldRecharge, 10)
	
	-- Generate loot
	local loot =
    {
        {rarity = Rarity(RarityType.Legendary), amount = 1},
        {rarity = Rarity(RarityType.Exotic), amount = 2},
        {rarity = Rarity(RarityType.Exceptional), amount = 3},
        {rarity = Rarity(RarityType.Rare), amount = 3},
        {rarity = Rarity(RarityType.Uncommon), amount = 4},
        {rarity = Rarity(RarityType.Common), amount = 6},
    }
	
    for _, p in pairs(loot) do
        for i = 1, p.amount do
			-- 60% upgrades, 40% weapons
			if math.random() > 0.4 then
				Loot(ship.index):insert(UpgradeGenerator():generateSectorSystem(x, y, p.rarity))
			else
				Loot(ship.index):insert(InventoryTurret(SectorTurretGenerator():generate(x, y, 0, p.rarity)))
			end
        end
    end

    AddDefaultShipScripts(ship)

    ship:addScript("ai/patrol.lua")
    ship:addScript("data/scripts/entity/XsotanDreadnought.lua")
    ship:addScript("story/xsotanbehaviour.lua")
    ship:setValue("is_xsotan", 1)
    ship:setValue("xsotan_dreadnought", 1) -- Support for carrier command priority setting
	
	Boarding(ship).boardable = false

    return ship
end

function createPlan(volume, material)
	local plan = PlanGenerator.makeXsotanShipPlan(volume, material)
	
	plan = planAddDreadlochSpecialties(plan)
	
	return plan
end

function planAddDreadlochSpecialties(plan)
	XDConfigLib.log(4, "Shields: "..plan:getStats().shield)
	-- Add shields to the ship when it does not have any
	if not plan:getStats().shield or plan:getStats().shield == 0 then
		local material = Material(MaterialType.Naonite)
		plan:addBlock(vec3(0, 0, 0), vec3(1, 1, 1), plan.rootIndex, -1, Color(), material, Matrix(), BlockType.ShieldGenerator) 
		XDConfigLib.log(3, "Add shields to plan")
	end
	
	return plan
end

function createShip(faction, name, plan, position, arrivalType)
	local ship = Sector():createShip(faction, name, plan, position, arrivalType)
	
	return ship
end


function sendCoordinates()
    invokeClientFunction(Player(callingPlayer), "receiveCoordinates", missionData.location, missionData.giverIndex, missionData.isEvent)
end
callable(nil, "sendCoordinates")

function onBossDestroyed()
	finish()
end


if onClient() then

	function receiveCoordinates(target_in, giver, event)
		if not event then 
			sync()
		end
		missionData.location = target_in
		missionData.giverIndex = giver
		missionData.isEvent = event
	end
	
	function getMissionDescription()
		if missionData.isEvent then
			return string.format("You received curious subspace signals by an unknown source. Their position is %i:%i."%_t, missionData.location.x, missionData.location.y)
		elseif missionData.giverIndex then
			return missionData.description%_t % missionData
		end
		XDConfigLib.log(3, "Could not determine mission desc")
		return "-"
	end
end


-- Should be moved to mission bulletin, but this doesnt seem possible without modding the bulletin
-- Maybe add it as a global function anywhere...
function awardRep(player, faction, baseAmount)
    local trust = (faction:getTrait("naive") + 2) / 2 -- 0.5 to 1.5

    local repAmount = baseAmount + baseAmount * ((0.5 - math.random()) / 2) -- make a random varity of +-25%
    repAmount = repAmount*trust

    Galaxy():changeFactionRelations(player, faction, repAmount)
end


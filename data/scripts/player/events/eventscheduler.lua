if onServer() then
	package.path = package.path .. ";data/scripts/lib/ConfigLib/?.lua"

	local ConfigLib = include ("ConfigLib")
	local XDConfigLib = ConfigLib("1721567838")
	
	
	if XDConfigLib.get("useEvent") then
		XDConfigLib.log(4, "Enable XsotanDreadnought event")
		table.insert(events, {schedule = random():getInt(50, 100) * 60, localEvent = false, script = "data/scripts/player/XsotanDreadnoughtAttack", minimum = 30 * 60, arguments = {true}, to = 400})
	end
end